import static spark.Spark.*;
public class HelloWorld{
	public static int convert(String str){
		int val = 0;
		val = Integer.parseInt(str);
		return val;
	}
	public static void main(String[] args){
		get("/hello", (req, res) -> "Hello World");

		get("/", (req, res) -> {
			return "<h1>Homepage</h1>";
		});
		
		get("/mult/:x/:y", (req, res) -> {
			int x = convert(req.params(":x"));
			int y = convert(req.params(":y"));
			return x * y;
		});
	}
}
