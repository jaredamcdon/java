#### Compile an app:
```sh
mvn compile
mvn clean compile
```

#### install
```sh
mvn install
```

#### Run an app:
```sh
mvn exec:java -Dexec.mainClass=HelloWorld
```
